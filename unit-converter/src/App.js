import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
	
constructor(props) {
    super(props);
    this.state = {
      fahrenheit: 0,
      kelvin: 0,
      celsius: 0
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const value = parseFloat(target.value);
    const name = target.name;

	
	
	switch (name) {
	  case 'fahrenheit':
  	  		var kelvinValue = (value + 459.67) * 5/9
	  		var celsiusValue = 5/9 *(value - 32);
	    break;
	  case 'kelvin':
	  		var celsiusValue = value - 273.15;
	  		var fahrenheitValue = 9/5 *(value - 273) + 32;
	    break;
	
	  case 'celsius':
	  		var kelvinValue = value + 273.15;
	  		var fahrenheitValue = 9/5 *(value) + 32;
	    break;
	  
	}
    this.setState({
      ['fahrenheit']: fahrenheitValue,
      ['kelvin']: kelvinValue,
      ['celsius']: celsiusValue
    });
  }
  
  handleSubmit(event) {
    alert('A name was submitted: ' + this.state.value);
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          fahrenheit:
          <input name="fahrenheit" type="text" value={this.state.fahrenheit} onChange={this.handleChange} />
        </label>
        <label>
          kelvin:
          <input name="kelvin" type="text" value={this.state.kelvin} onChange={this.handleChange} />
        </label>
        <label>
          celsius:
          <input name="celsius" type="text" value={this.state.celsius} onChange={this.handleChange} />
        </label>
        <input type="submit" value="Submit" />
      </form>
    );
  }
}
export default App;
